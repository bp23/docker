# Docker

[![Build Status](http://drone.lshay.cc/api/badges/bp/docker/status.svg?ref=refs/heads/main)](http://drone.lshay.cc/bp/docker)

The repo contains docker images we use at BP for building, testing, and running our code. Issues are open to be filed but the response time may not be fast as we are not focused on maintaining this for external users.

